# Salvation Army Canada Map

This tool was designed to extract thrift store locations from an API called from within the official [Salvation Army Canada](https://salvationarmy.ca/locator/) website and place them into KML format. 

## Dependencies
* Python 3.x
    * Requests module for making https requests
    * Simplekml for easily building KML files
    * JSON module for JSON-based geodata
* Also of course depends on official [Salvation Army Canada Locator API](https://webapp3.sallynet.org/api.locator/api/Locations/search/?countToReturn=999&serviceId=3) endpoint.
