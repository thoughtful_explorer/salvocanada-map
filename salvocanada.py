#!/usr/bin/python3

import requests
import simplekml
import json

#Set filename/path for KML file output
kmlfile = "salvocanada.kml"
#Set KML schema name
kmlschemaname = "salvocanada"
#Set page URL
pageURL = "https://webapp3.sallynet.org/api.locator/api/Locations/search/?countToReturn=999&serviceId=3"

#Gets all locations from the API
def getlocations(pageURL=pageURL):
    #Initialize a list of locations
    locations = []
    #Get the API response, and JSONify it
    #*NOTE* This is a strange API in that it fails SSL verification, so we must also disable it for this call
    response = requests.get(pageURL, verify=False).json()
    #Loop through all the stores in the response...
    for store in response:
    #...and append each to the locations list
        locations.append(store)
    return locations

#Example JSON from API calls:
'''
    {
        "$id": "227",
        "Id": 1033,
        "Name": "Yarmouth Thrift Store",
        "Street": "287 MAIN ST",
        "City": "YARMOUTH",
        "ProvinceCode": "NS",
        "Postal": "B5A 1E2   ",
        "Phone": "902 742-7749",
        "Fax": "",
        "Email": null,
        "Website": null,
        "Latitude": 43.8348913,
        "Longitude": -66.1199635
    },
'''
#Saves a KML file of a given list of stores
def createkml(stores):
    #Initialize kml object
    kml = simplekml.Kml()
    #Iterate through list for each store
    for store in stores:
        #Get the name of the store
        storename = store["Name"]
        #Get coordinates of the store
        lat = store["Latitude"]
        lng = store["Longitude"]
        #Create a full store address from the more granular address attributes
        storeaddress = str(store["Street"])+" "+str(store["City"])+", "+str(store["ProvinceCode"])+" "+str(store["Postal"])
        #First, create the point name and description (using the address as the description) in the kml
        point = kml.newpoint(name=storename,description=storeaddress)
        #Finally, add coordinates to the feature
        point.coords=[(lng,lat)]
    #Save the final KML file
    kml.save(kmlfile)

#Bring it all together
createkml(getlocations())
